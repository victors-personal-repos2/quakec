TODO
===

* Add support for more types and type extensions
* Add support for common snippets
* Add support for function outline and ctrl+click
* Add colored brackets support
* Add auto formatter
* Add build and run profiles